﻿using System;

namespace Calculator.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var calculator = CalculatorService.GetInstance();
            
            Console.WriteLine(calculator.Compute('/', 5, 0));
        }
    }
}