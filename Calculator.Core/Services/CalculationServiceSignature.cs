using System;
using System.Collections.Generic;

public partial class CalculatorService
{
    private static CalculatorService instance;

    //NEW FEATURE
    private Dictionary<char, Func<CalculatorDTO, decimal>> _operations = new()
    {
        {'+', value => value.a + value.b},
        {'-', value => value.a - value.b},
        {'*', value => value.a * value.b},
        {
            '/', value =>
            {
                //NEW FEATURE
                if (value is {b: 0})
                    throw new ArgumentException();
                return value.a / value.b;
            }
        },
    };

    public static partial CalculatorService GetInstance();

    private CalculatorService()
    {
    }
}