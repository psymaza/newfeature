using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

//NEW FEATURE
public sealed record CalculatorDTO(decimal a, decimal b);

public partial class CalculatorService
{
    //NEW FEATURE
    [ModuleInitializer]
    public static void OperationsInitializer()
    {
        GetInstance();
    }

    //NEW FEATURE
    public decimal Compute(char operation, decimal a, decimal b)
        => operation switch
        {
            '+' => _operations[operation](new CalculatorDTO(a, b)),
            '-' => _operations[operation](new CalculatorDTO(a, b)),
            '*' => _operations[operation](new CalculatorDTO(a, b)),
            '/' => _operations[operation](new CalculatorDTO(a, b)),
            ':' => _operations[operation](new CalculatorDTO(a, b)),
            _ => throw new InvalidOperationException()
        };

    //NEW FEATURE
    public static partial CalculatorService GetInstance()
        => instance ??= new CalculatorService();
}